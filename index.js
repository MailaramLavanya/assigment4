let input = document.getElementById('inputBox');
let buttons = document.querySelectorAll('button');

let string = "";
let arr = Array.from(buttons);
arr.forEach(button => {
    button.addEventListener('click', (element) =>{
        if(element.target.innerHTML == '='){
            string = eval(string);
            input.value = string;
        }

        else if(element.target.innerHTML == 'C'){
            string = "";
            input.value = string;
        }
        else if(element.target.innerHTML == 'DEL'){
            string = string.substring(0, string.length-1);
            input.value = string;
        }
        else{
            string += element.target.innerHTML;
            input.value = string;
        }
        
    })
})